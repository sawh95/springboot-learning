package zuhlke.bigTest.domain;

import org.junit.jupiter.api.Test;
import zuhlke.bigTest.domain.predicates.RankPredicate;
import zuhlke.bigTest.domain.predicates.RankIs;
import zuhlke.bigTest.domain.predicates.RolePredicate;

import java.util.List;
import java.util.function.Predicate;

import static java.util.Arrays.asList;
import static java.util.Collections.EMPTY_LIST;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static zuhlke.bigTest.domain.PlayerFilter.and;
import static zuhlke.bigTest.domain.PlayerFilter.or;
import static zuhlke.bigTest.domain.Rank.*;
import static zuhlke.bigTest.domain.Roles.*;

class PlayerRepositoryTest {

    private final Player wheeler = new Player("Wheeler", top, Diamond);
    private final Player montyRock = new Player("MontyRock", jungle, Platinum);
    private final Player blueHawkSabo = new Player("BlueHawkSabo", middle, Platinum);
    private final Player ladyLuck = new Player("LadyLuck", carry, Silver);
    private final Player spoff = new Player("Spoff", support, Diamond);


    @Test
    void repositoryOnlyContainsPlayersWhoWereAdded() {
        PlayerRepository repo = new PlayerRepository();
        assertEquals(EMPTY_LIST, repo.getPlayers());

        Player ladyLuck = new Player("Lady luck", middle, Silver);
        repo.add(ladyLuck);
        assertEquals(singletonList(ladyLuck), repo.getPlayers());

        Player wheeler = new Player("Wheeler", top, Diamond);
        repo.add(wheeler);
        assertEquals(asList(ladyLuck, wheeler), repo.getPlayers());
    }

    @Test
    void appliesListOfFilters() {
        PlayerRepository repo = new PlayerRepository();
        addExamplePlayers(repo);
        Predicate<Player> roleFilter = new RolePredicate(carry);
        Predicate<Player> rankFilter = new RankPredicate(RankIs.lessThan, Gold);

        List<Player> players = repo.getFilteredPlayers(and(asList(roleFilter, rankFilter)));

        assertEquals(singletonList(ladyLuck), players);
    }

    @Test
    void appliesFiltersEitherOr() {
        PlayerRepository repo = new PlayerRepository();
        addExamplePlayers(repo);
        Predicate<Player> roleFilter = new RolePredicate(top);
        Predicate<Player> rankFilter = new RankPredicate(RankIs.lessThan, Gold);

        List<Player> players = repo.getFilteredPlayers(or(asList(roleFilter, rankFilter)));

        assertTrue(players.contains(ladyLuck));
        assertTrue(players.contains(wheeler));
        assertEquals(2, players.size());
    }

    @Test
    void combinesAndOrConditions() {
        PlayerRepository repo = new PlayerRepository();
        addExamplePlayers(repo);
        Predicate<Player> roleFilter = new RolePredicate(carry);
        Predicate<Player> rankFilter = new RankPredicate(RankIs.equalToOrGreaterThan, Gold);
        Predicate<Player> dumbFilter = (Player player) -> player.getName().contains("Luck");

        List<Player> players = repo.getFilteredPlayers(PlayerFilter.andOr(asList(dumbFilter), asList(roleFilter, rankFilter)));

        assertTrue(players.contains(ladyLuck));
        assertEquals(1, players.size());
    }

    private void addExamplePlayers(PlayerRepository repo) {
        repo.add(wheeler);
        repo.add(montyRock);
        repo.add(blueHawkSabo);
        repo.add(ladyLuck);
        repo.add(spoff);
    }
}

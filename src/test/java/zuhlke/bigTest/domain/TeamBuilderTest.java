package zuhlke.bigTest.domain;

import org.junit.jupiter.api.Test;
import zuhlke.bigTest.domain.Exceptions.NoValidTeamException;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static zuhlke.bigTest.domain.Rank.*;
import static zuhlke.bigTest.domain.Roles.*;

class TeamBuilderTest {

    private final List<Player> aTeam = asList(
            new Player("Wheeler", top, Diamond),
            new Player("Fettes", jungle, Platinum),
            new Player("BlueHawkSabo", middle, Platinum),
            new Player("Spoff", carry, Diamond),
            new Player("ActuallyIsGooby", support, Gold)
    );

    @Test
    void throwsExceptionWhenThereArent5Players() {
        PlayerRepository playerRepository = mock(PlayerRepository.class);
        when(playerRepository.getPlayers()).thenReturn(Collections.emptyList());
        TeamBuilder teamBuilder = new TeamBuilder(playerRepository);

        assertThrows(NoValidTeamException.class, teamBuilder::formTeam);
    }

    @Test
    void assignsFivePlayersIntoRoles() throws NoValidTeamException {
        List<Player> expectedTeam = aTeam;
        PlayerRepository players = mock(PlayerRepository.class);
        when(players.getPlayers()).thenReturn(expectedTeam);
        TeamBuilder teamBuilder = new TeamBuilder(players);

        Team team = teamBuilder.formTeam();

        assertEquals(expectedTeam, team.players);
    }

    @Test
    void generatesATeamName() throws NoValidTeamException {
        PlayerRepository players = mock(PlayerRepository.class);
        when(players.getPlayers()).thenReturn(aTeam);
        TeamBuilder teamBuilder = new TeamBuilder(players);

        Team team = teamBuilder.formTeam();

        assert (team.name.length() > 0);
    }
}

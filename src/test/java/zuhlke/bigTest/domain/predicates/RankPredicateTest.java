package zuhlke.bigTest.domain.predicates;

import org.junit.jupiter.api.Test;
import zuhlke.bigTest.domain.Player;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;
import static zuhlke.bigTest.domain.Rank.*;
import static zuhlke.bigTest.domain.Rank.Diamond;
import static zuhlke.bigTest.domain.Roles.*;

class RankPredicateTest {
    private final Player bronzie = new Player("Wheeler", top, Bronze);
    private final Player silverBoi = new Player("MontyRock", jungle, Silver);
    private final Player goldieLocks = new Player("BlueHawkSabo", middle, Gold);
    private final Player plutsie = new Player("LadyLuck", carry, Platinum);
    private final Player dee = new Player("Spoff", support, Diamond);

    @Test
    void getsAllPlayersEqualOrGreaterThanAGivenRank() {
        List<Player> players = asList(bronzie, silverBoi, goldieLocks, plutsie, dee);
        Predicate<Player> rankFilter = new RankPredicate(RankIs.equalToOrGreaterThan, Gold);

        List<Player> goldAndAbove = players.stream().filter(rankFilter).collect(Collectors.toList());

        assertTrue(goldAndAbove.contains(goldieLocks));
        assertTrue(goldAndAbove.contains(plutsie));
        assertTrue(goldAndAbove.contains(dee));
    }

    @Test
    void getAllPlayersEqualToGivenRank() {
        List<Player> players = asList(bronzie, plutsie);
        Predicate<Player> filter = new RankPredicate(RankIs.equalTo, Platinum);

        List<Player> platPlayers = players.stream().filter(filter).collect(Collectors.toList());

        assertTrue(platPlayers.contains(plutsie));
        assertEquals(1, platPlayers.size());
    }

    @Test
    void getPlayersWithLessThanTheGivenRank() {
        List<Player> players = asList(bronzie, plutsie);
        Predicate<Player> filter = new RankPredicate(RankIs.lessThan, Platinum);

        List<Player> platPlayers = players.stream().filter(filter).collect(Collectors.toList());

        assertTrue(platPlayers.contains(bronzie));
        assertEquals(1, platPlayers.size());
    }
}

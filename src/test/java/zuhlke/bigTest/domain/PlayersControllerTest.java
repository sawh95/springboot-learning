package zuhlke.bigTest.domain;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import zuhlke.bigTest.persistence.CrudRepository;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static zuhlke.bigTest.domain.Rank.Diamond;
import static zuhlke.bigTest.domain.Roles.*;

@WebMvcTest
class PlayersControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    PlayerRepository repository;
    @MockBean
    WordSupplier supplier;
    @MockBean
    CrudRepository repo;

    @Test
    void getTheListOfPlayers() throws Exception {
        when(repository.getPlayers()).thenReturn(singletonList(new Player("Wheeler", top, Diamond)));

        mockMvc.perform(get("/players"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("name")))
                .andExpect(content().string(containsString("Wheeler")))
                .andExpect(content().string(containsString("role")))
                .andExpect(content().string(containsString("top")));

    }

    @Test
    void addANewPlayer() throws Exception {
        mockMvc.perform(post("/players").contentType("application/json").content("{\"name\":\"Wheeler\", \"role\":\"top\"}")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("name")))
                .andExpect(content().string(containsString("Wheeler")))
                .andExpect(content().string(containsString("role")))
                .andExpect(content().string(containsString("top")));
    }

    @Test
    void filterByRole() throws Exception {
        when(repository.getFilteredPlayers(any(PlayerFilter.class)))
                .thenReturn(singletonList(new Player("Dave", carry, Rank.GrandMaster)));

        String filter = "{ \"field\": \"role\", \"operator\": \"equalTo\", \"value\": \"carry\" }";

        mockMvc.perform(post("/filter").contentType("application/json").content(filter))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("name")))
                .andExpect(content().string(containsString("Dave")))
                .andExpect(content().string(containsString("role")))
                .andExpect(content().string(containsString("carry")));
    }
}

package zuhlke.bigTest.controller;

import org.junit.jupiter.api.Test;
import zuhlke.bigTest.domain.*;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

class RawFilterTest {

    @Test
    void buildsRankFilter() {
        PlayerRepository repo = new PlayerRepository();
        Player trashPlayer = new Player("trashy", Roles.carry, Rank.Bronze);
        Player reallyTrashPlayer = new Player("trashier", Roles.carry, Rank.Iron);
        repo.add(trashPlayer);
        repo.add(reallyTrashPlayer);
        RawFilter rawFilter = new RawFilter("rank", "lessThan", "Silver");

        PlayerFilter filter = rawFilter.constructPlayerFilter();

        List<Player> filteredPlayers = repo.getFilteredPlayers(filter);
        assertTrue(filteredPlayers.contains(trashPlayer));
        assertTrue(filteredPlayers.contains(reallyTrashPlayer));
        assertEquals(2, filteredPlayers.size());
    }

    @Test
    void buildsRoleFilter() {
        PlayerRepository repo = new PlayerRepository();
        Player leader = new Player("leader", Roles.carry, Rank.Iron);
        repo.add(leader);
        repo.add(new Player("supportive", Roles.support, Rank.Bronze));
        RawFilter rawFilter = new RawFilter("role", "equalTo", "carry");

        PlayerFilter filter = rawFilter.constructPlayerFilter();

        List<Player> filteredPlayers = repo.getFilteredPlayers(filter);
        assertTrue(filteredPlayers.contains(leader));
        assertEquals(1, filteredPlayers.size());
    }

}

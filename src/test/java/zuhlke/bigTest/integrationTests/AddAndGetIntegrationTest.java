package zuhlke.bigTest.integrationTests;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import zuhlke.bigTest.domain.Player;
import zuhlke.bigTest.domain.Rank;
import zuhlke.bigTest.domain.Roles;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AddAndGetIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate template;

    @Test
    void addingANewPlayerIncludesThemInTheReturn() {
        assertEquals("[]", template.getForObject("http://localhost:" + port + "/players", String.class));

        Player newPlayer = new Player("Spoff", Roles.support, Rank.Diamond);

        template.postForObject("http://localhost:" + port + "/players", newPlayer, String.class);

        assertEquals(newPlayer, template.getForObject("http://localhost:" + port + "/players", Player[].class)[0]);
    }
}

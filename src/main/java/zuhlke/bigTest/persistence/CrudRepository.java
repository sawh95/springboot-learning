package zuhlke.bigTest.persistence;

import org.springframework.data.jpa.repository.Query;
import zuhlke.bigTest.domain.Player;
import zuhlke.bigTest.domain.Rank;

import java.util.List;
import java.util.Optional;

public interface CrudRepository extends org.springframework.data.repository.CrudRepository<Player, Long> {

    List<Player> findByRank(Rank rank);

    Optional<Player> findById(Long id);

    List<Player> findByNameContainingAndRankGreaterThan(String nameSubset, Rank rank);

    @Query("SELECT u from Player u where u.name like %?1 or u.name = 'Cpt Calum Neil Wheeler'")
    List<Player> theOGs(String param);
}

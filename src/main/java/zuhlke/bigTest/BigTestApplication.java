package zuhlke.bigTest;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import zuhlke.bigTest.persistence.CrudRepository;
import zuhlke.bigTest.domain.Player;
import zuhlke.bigTest.domain.Rank;
import zuhlke.bigTest.domain.Roles;

@SpringBootApplication
@EnableCaching
public class BigTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BigTestApplication.class, args);
	}

	@Bean
	public CommandLineRunner preLoad(CrudRepository repo) {
		return args -> {
			repo.save(new Player("Cpt Calum Neil Wheeler", Roles.top, Rank.Diamond));
			repo.save(new Player("Sir tilts a lot", Roles.middle, Rank.Platinum));
			repo.save(new Player("The reason we can't play flex", Roles.carry, Rank.Silver));
			repo.save(new Player("What do you mean Bard isn't a good pick here", Roles.support, Rank.Gold));
			repo.save(new Player("Loves role playing as a real man", Roles.jungle, Rank.Gold));
			repo.save(new Player("Why are all my friends trash", Roles.support, Rank.Diamond));
			repo.save(new Player("Forgot you play Cho, fair enough", Roles.top, Rank.Diamond));

			repo.findAll().forEach(System.out::println);
		};
	}

}

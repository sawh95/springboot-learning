package zuhlke.bigTest.domain;

import java.util.List;

public class Team {
    public List<Player> players;
    public String name;

    public Team(List<Player> players, String name) {
        this.players = players;
        this.name = name;
    }
}

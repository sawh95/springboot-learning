package zuhlke.bigTest.domain;


import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
public class PlayerRepository {

    private final List<Player> players = new ArrayList<>();

    @Cacheable("players")
    public List<Player> getPlayers() {
        return players;
    }

    public void add(Player newPlayer) {
        players.add(newPlayer);
    }

    public List<Player> getFilteredPlayers(PlayerFilter playerFilter) {
        return players.stream()
                .filter(playerFilter.orPredicates.stream().reduce(x -> playerFilter.orPredicates.size() == 0, Predicate::or))
                .filter(playerFilter.andPredicates.stream().reduce(x -> true, Predicate::and))
                .collect(Collectors.toList());
    }
}

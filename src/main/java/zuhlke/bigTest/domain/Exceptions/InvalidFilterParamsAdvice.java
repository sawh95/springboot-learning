package zuhlke.bigTest.domain.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class InvalidFilterParamsAdvice {

    @ResponseBody
    @ExceptionHandler(InvalidFilterParamsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String invalidFilterParamHandler(InvalidFilterParamsException ex) {
        return ex.getMessage();
    }
}

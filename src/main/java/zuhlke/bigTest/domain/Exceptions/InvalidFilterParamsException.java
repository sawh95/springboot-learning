package zuhlke.bigTest.domain.Exceptions;

public class InvalidFilterParamsException extends RuntimeException{

    public InvalidFilterParamsException(String param) {
        super("Invalid filter parameter: " + param);
    }
}

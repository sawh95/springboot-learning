package zuhlke.bigTest.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Player {

    @Id
    @GeneratedValue
    private long id;
    private String name;
    private Roles role;
    private Rank rank;

    protected Player() {
    }

    public Player(String name, Roles role, Rank rank) {
        this.name = name;
        this.role = role;
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public Roles getRole() {
        return role;
    }

    public Rank getRank() {
        return rank;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", role=" + role +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(name, player.name) &&
                role == player.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, role);
    }
}

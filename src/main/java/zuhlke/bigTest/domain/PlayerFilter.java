package zuhlke.bigTest.domain;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

public class PlayerFilter {
    final List<Predicate<Player>> orPredicates;
    final List<Predicate<Player>> andPredicates;

    private PlayerFilter(List<Predicate<Player>> orPredicates, List<Predicate<Player>> andPredicates) {
        this.orPredicates = orPredicates;
        this.andPredicates = andPredicates;
    }

    public static PlayerFilter andOr(List<Predicate<Player>> andPredicates, List<Predicate<Player>> orPredicates) {
        return new PlayerFilter(orPredicates, andPredicates);
    }

    public static PlayerFilter and(List<Predicate<Player>> andPredicates) {
        return new PlayerFilter(Collections.emptyList(), andPredicates);
    }

    public static PlayerFilter or(List<Predicate<Player>> orPredicates) {
        return new PlayerFilter(orPredicates, Collections.emptyList());
    }
}

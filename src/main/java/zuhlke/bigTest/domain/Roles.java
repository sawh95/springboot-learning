package zuhlke.bigTest.domain;

public enum Roles {
    top("top"), jungle("jungle"), middle("middle"), carry("carry"), support("support");

    private String role;

    Roles(String role) {
        this.role = role;
    }

    public static Roles fromString(String roleName) {
        return Roles.valueOf(roleName);
    }
}

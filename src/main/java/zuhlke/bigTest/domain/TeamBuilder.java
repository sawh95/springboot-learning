package zuhlke.bigTest.domain;

import zuhlke.bigTest.domain.Exceptions.NoValidTeamException;

import java.util.List;

public class TeamBuilder {

    private final PlayerRepository playerRepository;

    public TeamBuilder(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public Team formTeam() throws NoValidTeamException {
        List<Player> players = playerRepository.getPlayers();
        if (players.size() < 5) {
            throw new NoValidTeamException();
        }

        return new Team(players, generateTeamName());
    }

    private String generateTeamName() {
        return "Name";
    }
}

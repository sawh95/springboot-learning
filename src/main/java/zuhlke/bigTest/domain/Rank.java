package zuhlke.bigTest.domain;

public enum Rank {
    Iron(0),
    Bronze(1),
    Silver(2),
    Gold(3),
    Platinum(4),
    Diamond(5),
    Master(6),
    GrandMaster(7),
    Challenger(8);

    public int value;

    Rank(int value) {
        this.value = value;
    }

    public static Rank fromString(String rank) {
        switch (rank) {
            case "Iron":
                return Iron;
            case "Bronze":
                return Bronze;
            case "Silver":
                return Silver;
            case "Gold":
                return Gold;
            case "Platinum":
                return Platinum;
            case "Diamond":
                return Diamond;
            default:
                return Master;
        }
    }
}

package zuhlke.bigTest.domain.predicates;

import zuhlke.bigTest.domain.Player;
import zuhlke.bigTest.domain.Rank;

import java.util.function.BiFunction;
import java.util.function.Predicate;

public class RankPredicate implements Predicate<Player> {

    private final BiFunction<Rank, Player, Boolean> operation;
    private final Rank rank;

    public RankPredicate(RankIs operation, Rank rank) {
        this.operation = operation.op;
        this.rank = rank;
    }

    @Override
    public boolean test(Player player) {
        return operation.apply(rank, player);
    }
}

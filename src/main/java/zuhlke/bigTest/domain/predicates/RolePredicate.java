package zuhlke.bigTest.domain.predicates;

import zuhlke.bigTest.domain.Player;
import zuhlke.bigTest.domain.Roles;

import java.util.function.Predicate;

public class RolePredicate implements Predicate<Player> {
    private final Roles role;

    public RolePredicate(Roles role) {
        this.role = role;
    }

    @Override
    public boolean test(Player player) {
        return player.getRole() == role;
    }
}

package zuhlke.bigTest.domain.predicates;

import zuhlke.bigTest.domain.Player;
import zuhlke.bigTest.domain.Rank;

import java.util.function.BiFunction;

public enum RankIs {
    equalToOrGreaterThan((Rank rank, Player player) -> player.getRank().value >= rank.value),
    equalTo((Rank rank, Player player) -> player.getRank().value == rank.value),
    lessThan((Rank rank, Player player) -> player.getRank().value < rank.value);

    public BiFunction<Rank, Player, Boolean> op;

    RankIs(BiFunction<Rank, Player, Boolean> op) {
        this.op = op;
    }
}

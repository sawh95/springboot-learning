package zuhlke.bigTest.domain;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

@Service
public class WordSupplier {

    private final List<String> someWords = asList(
            "Florentine",
            "Serendipitous",
            "Malingering",
            "Twirl",
            "Swill",
            "Mystery",
            "Honorific",
            "Legion",
            "Tactile"
    );

    @Cacheable("goodWords")
    public String getWordRatedAt(Integer rating) {
        simulateSlowService();
        return someWords.get(rating-1);
    }

    private void simulateSlowService() {
        try {
            long time = 3000L;
            Thread.sleep(time);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}

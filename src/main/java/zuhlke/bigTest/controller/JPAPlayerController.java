package zuhlke.bigTest.controller;

import org.springframework.web.bind.annotation.*;
import zuhlke.bigTest.domain.Player;
import zuhlke.bigTest.domain.Rank;
import zuhlke.bigTest.persistence.CrudRepository;

import java.util.Comparator;
import java.util.List;

@RestController
public class JPAPlayerController {

    private CrudRepository repo;

    public JPAPlayerController(CrudRepository repo) {
        this.repo = repo;
    }

    @GetMapping("/jpaPlayers")
    Iterable<Player> getAllThePLayers() {
        return repo.findAll();
    }

    @GetMapping("/jpaPlayers/rank")
    List<Player> getByRank(@RequestParam String rank) {
        return repo.findByRank(Rank.fromString(rank));
    }

    @PostMapping("/jpaPlayers")
    Player addAPlayer(@RequestBody Player newPLayer) {
        return repo.save(newPLayer);
    }

    @GetMapping("/a")
    List<Player> weirdQuery(@RequestParam String nameFragment, @RequestParam Rank rank){
        Comparator<Player> comparator = Comparator.comparingInt((Player player) -> player.getRank().value);

        List<Player> players = repo.findByNameContainingAndRankGreaterThan(nameFragment, rank);
        players.sort(comparator);
        return players;
    }

    @GetMapping("/b")
    List<Player> yes() {
        return repo.theOGs("q");
    }

}

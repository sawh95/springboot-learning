package zuhlke.bigTest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import zuhlke.bigTest.domain.Player;
import zuhlke.bigTest.domain.PlayerRepository;

import java.util.List;

@RestController
public class PlayersController {

    private final PlayerRepository playerRepository;

    public PlayersController(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @GetMapping("/players")
    List<Player> allPlayers() {
        return playerRepository.getPlayers();
    }

    @PostMapping("/players")
    Player newPlayer(@RequestBody Player newPlayer) {
        playerRepository.add(newPlayer);
        return newPlayer;
    }

    @PostMapping("/filter")
    List<Player> filteredPlayers(@RequestBody RawFilter rawFilter) {
        return playerRepository.getFilteredPlayers(rawFilter.constructPlayerFilter());
    }
}

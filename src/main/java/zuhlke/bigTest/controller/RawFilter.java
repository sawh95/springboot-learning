package zuhlke.bigTest.controller;

import zuhlke.bigTest.domain.Exceptions.InvalidFilterParamsException;
import zuhlke.bigTest.domain.PlayerFilter;
import zuhlke.bigTest.domain.Rank;
import zuhlke.bigTest.domain.Roles;
import zuhlke.bigTest.domain.predicates.RankIs;
import zuhlke.bigTest.domain.predicates.RankPredicate;
import zuhlke.bigTest.domain.predicates.RolePredicate;

import static java.util.Collections.singletonList;
import static zuhlke.bigTest.domain.Rank.*;

public class RawFilter {
    private final String field;
    private final String operator;
    private final String value;

    public RawFilter(String field, String operator, String value) {
        this.field = field;
        this.operator = operator;
        this.value = value;
    }

    public PlayerFilter constructPlayerFilter() {
        if (field.equals("role")) {
            return PlayerFilter.and(singletonList(new RolePredicate(Roles.fromString(value))));
        } else {
            RankIs operation;
            switch (operator) {
                case "lessThan":
                    operation = RankIs.lessThan;
                    break;
                default:
                    throw new InvalidFilterParamsException(operator);
            }
            Rank rank;
            switch (value) {
                case "Silver":
                    rank = Silver;
                    break;
                default:
                    throw new InvalidFilterParamsException(value);
            }

            System.out.println(operator + " <-Operation|Value -> " + rank);
            return PlayerFilter.and(singletonList(new RankPredicate(operation, rank)));
        }
    }
}

package zuhlke.bigTest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import zuhlke.bigTest.domain.WordSupplier;

@RestController
public class CacheTestController {

    private final WordSupplier faveWordSuplier;

    public CacheTestController(WordSupplier faveWordSuplier) {
        this.faveWordSuplier = faveWordSuplier;
    }

    @GetMapping("/word")
    String getFaveWord(@RequestParam Integer rating) {
        return faveWordSuplier.getWordRatedAt(rating);
    }
}
